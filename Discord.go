package DiscordProvider

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/cospoland/Gopher/GopherDK"
)

type providerMethods struct {
	Module *GopherDK.Module
}

func (s providerMethods) Start() bool {
	fmt.Println(s.Module.Configuration["Token"])
	fmt.Println(s.Module.Configuration)
	fmt.Println(s.Module)
	discord, err := discordgo.New("Bot " + s.Module.Configuration["Token"])
	if err != nil {
		panic(err)
	}
	s.Module.Extra["Discord"] = discord
	discord.AddHandler(s.MakeContext)
	err = discord.Open()
	if err != nil {
		panic(err)
	}
	return false
}

func (s providerMethods) Mention(User string) string {
	return fmt.Sprintf("<@%s>", User)
}

func (pm providerMethods) MakeContext(s *discordgo.Session, m *discordgo.MessageCreate) {
	fmt.Println(m)
}

// TODO: Implement
func (s providerMethods) Send(Context GopherDK.MessageContext, User string) bool {
	return false
}

func New() *GopherDK.Module {
	mod := &GopherDK.Module{}
	mod.Name = "Discord"
	mod.Version = "v-git::master"
	mod.Author = "Pyy"
	mod.Commands = map[string]interface{}{}
	mod.Extra = map[string]interface{}{}
	mod.Methods = providerMethods{Module: mod}
	return mod
}
